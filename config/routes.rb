Rails.application.routes.draw do
  #resources :represents
  resources :departments
  resources :courses
  resources :tutorials
  resources :tags
  resources :services#, except: :create #per evitare che mi risponda create al posto di find_near_services con metodo POST
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :user_sessions, only: [:create, :destroy]
  resources :users, only: [:index, :new, :create]
  resources :pages, only: [:index]
  #root to: 'tags#index'
  get '/', to: 'tags#index'
  get 'tags', to: 'tags#index'
  get 'tutorials', to:'tutorials#index'
  get 'services', to:'services#index'
  get 'result', to: 'tags#index'
  post 'result', to: 'courses#result'
  post 'servicesa.json', to: 'services#find_near_services'
  get 'departments.json', to: 'departments#index'
  delete 'sign_out', to: 'user_sessions#destroy', as: :sign_out
  get 'sign_in', to: 'user_sessions#new', as: :sign_in
end
