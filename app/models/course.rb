class Course < ApplicationRecord
    belongs_to :department, optional: true
    has_many :represents
    has_many :tags, through: :represents

    def self.find_course(tags)
        if tags.count >= 5
            courses = Course.joins(:tags).where(:tags => { :id => [tags] })
                .having("count(tags.name) >= ?", tags.count - 2).group('courses.id')
        elsif tags.count >= 3
            courses = Course.joins(:tags).where(:tags => { :id => [tags] })
                .having("count(tags.name) >= ?", tags.count - 1).group('courses.id')
        else
            courses = Course.joins(:tags).where(:tags => { :id => [tags] })
                .having("count(tags.name) = ?", tags.count).group('courses.id')
        end
    end

    def self.random_course()
        random_course = Course.order("RANDOM()").all.limit(2)
    end
end
