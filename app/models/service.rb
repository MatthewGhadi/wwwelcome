class Service < ApplicationRecord
    has_many :connects
    has_many :tags, through: :connects

    def self.find_services_by_department_lat_lng(latitude, longitude)
        if(latitude[0] == '-')
            lat = latitude[0..5]
        else
            lat = latitude[0..4]
        end
        if(longitude[0] == '-')
            lng = longitude[0..5]
        else
            lng = longitude[0..4]
        end
        lat = lat + '%'
        lng = lng + '%'
        services1 = Service.where("latitude LIKE ? AND longitude LIKE ?", lat, lng)
        services = services1
        n_serv = services1.count
        if(n_serv <= 2)
            new_lng = longitude.to_f
            new_lng += 0.01
            new_lng = new_lng.to_s
            if(new_lng[0] == '-')
                n_lng = new_lng[0..5]
            else
                n_lng = new_lng[0..4]
            end
            n_lng = n_lng + '%'
            services2 = Service.where("latitude LIKE ? AND longitude LIKE ?", lat, n_lng)
            services = services1 | services2
            n_serv = services2.count
            #da deccomentare qual'ora si sia in un'altro meridiano
            # if(n_serv <= 2)
            #     new_lat = latitude.to_f
            #     new_lat += 0.01
            #     new_lat = new_lat.to_s
            #     if(new_lat[0] == '-')
            #         n_lat = new_lat[0..5]
            #     else
            #         n_lat = new_lat[0..4]
            #     end
            #     n_lat = n_lat + '%'
            #     services3 = Service.where("latitude LIKE ? AND longitude LIKE ?", n_lat, lng)
            #     services = services1 | services2 | services3
            # end
        end
        return services
    end
end