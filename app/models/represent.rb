class Represent < ApplicationRecord
    belongs_to :tag, optional: true
    belongs_to :course, optional: true
end
