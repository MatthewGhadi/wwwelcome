class Tag < ApplicationRecord
    has_many :represent
    has_many :courses, through: :represent
    has_many :connects
    has_many :services, through: :connects
    has_many :maps
    has_many :tutorials, through: :maps
end
