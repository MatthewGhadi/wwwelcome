class Tutorial < ApplicationRecord
    has_many :maps
    has_many :tags, through: :maps

    #ordino i tutorial in ordine alfabetico in base al titolo
    def self.order_tutorial
        tutorials = Tutorial.order(title: :asc)
    end
end
