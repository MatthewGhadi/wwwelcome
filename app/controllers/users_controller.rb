class UsersController < ApplicationController
    def index
        @users = User.all
    end

    def new
        @user = User.new
    end

    def create
        @user = User.new(users_params.to_h)
        if @user.save
        flash[:success] = "Account registered!"
        redirect_to pages_path
        else
        render :new
        end
    end

    private

    def users_params
        params.require(:user).permit(:email, :password, :password_confirmation)
    end
end
