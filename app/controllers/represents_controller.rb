class RepresentsController < ApplicationController
  before_action :set_represent, only: [:show, :edit, :update, :destroy]

  # GET /represents
  # GET /represents.json
  def index
    @represents = Represent.all
  end

  # GET /represents/1
  # GET /represents/1.json
  def show
  end

  # GET /represents/new
  def new
    @represent = Represent.new
  end

  # GET /represents/1/edit
  def edit
  end

  # POST /represents
  # POST /represents.json
  def create
    @represent = Represent.new(represent_params)

    respond_to do |format|
      if @represent.save
        format.html { redirect_to @represent, notice: 'Represent was successfully created.' }
        format.json { render :show, status: :created, location: @represent }
      else
        format.html { render :new }
        format.json { render json: @represent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /represents/1
  # PATCH/PUT /represents/1.json
  def update
    respond_to do |format|
      if @represent.update(represent_params)
        format.html { redirect_to @represent, notice: 'Represent was successfully updated.' }
        format.json { render :show, status: :ok, location: @represent }
      else
        format.html { render :edit }
        format.json { render json: @represent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /represents/1
  # DELETE /represents/1.json
  def destroy
    @represent.destroy
    respond_to do |format|
      format.html { redirect_to represents_url, notice: 'Represent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_represent
      @represent = Represent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def represent_params
      params.require(:represent).permit(:course_id, :tag_id)
    end
end
