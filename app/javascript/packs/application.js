/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

console.log('Hello World from Webpacker')

import TurbolinksAdapter from 'vue-turbolinks'
import Vue from 'vue/dist/vue.esm'
import ServiceList from '../serviceList.vue'
import Googlemap from '../googlemap.vue'
import * as VueGoogleMaps from "vue2-google-maps";

import TagsList from '../tagsList.vue'
import Tag from '../tag.vue'
import Result from '../result.vue'
import Course from '../course.vue'
import TutorialList from '../tutorialList.vue'
import Tutorial from '../tutorial.vue'
import TutorialDescription from '../tutorialDescription.vue'
import Service from '../service.vue'

window.axios = require('axios');

Vue.use(TurbolinksAdapter)
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDlwdCJJ7cmhz4KRHsRSDMKWNBnTxFpTZY",
    libraries: "places" // necessary for places input
  }
});

Vue.component('googlemap', Googlemap)
Vue.component('service-list', ServiceList)
Vue.component('tags-list', TagsList)
Vue.component('tag', Tag)
Vue.component('result', Result)
Vue.component('course', Course)
Vue.component('tutorial-list', TutorialList)
Vue.component('tutorial', Tutorial)
Vue.component('tutorial-description', TutorialDescription)
Vue.component('service', Service)

export const BUS = new Vue()

document.addEventListener('turbolinks:load', () => {
  const vm = new Vue({
    el: '#app',
  })
})