json.extract! service, :id, :name, :office, :site, :created_at, :updated_at
json.url service_url(service, format: :json)
