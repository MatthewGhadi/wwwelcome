json.extract! course, :id, :name, :course_class, :site, :access, :students, :duration, :created_at, :updated_at
json.url course_url(course, format: :json)
