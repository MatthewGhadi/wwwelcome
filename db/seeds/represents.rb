Represent.destroy_all

# INGEGNERIA #
Represent.create!(course_id: 2, tag_id: 1)
Represent.create!(course_id: 2, tag_id: 2)
Represent.create!(course_id: 2, tag_id: 3)
Represent.create!(course_id: 1, tag_id: 4)
Represent.create!(course_id: 1, tag_id: 5)
Represent.create!(course_id: 3, tag_id: 8)
Represent.create!(course_id: 3, tag_id: 9)
Represent.create!(course_id: 3, tag_id: 10)
Represent.create!(course_id: 4, tag_id: 4)
Represent.create!(course_id: 4, tag_id: 5)
Represent.create!(course_id: 4, tag_id: 6)
Represent.create!(course_id: 4, tag_id: 7)
Represent.create!(course_id: 5, tag_id: 2)
Represent.create!(course_id: 5, tag_id: 11)
Represent.create!(course_id: 6, tag_id: 1)
Represent.create!(course_id: 6, tag_id: 2)
Represent.create!(course_id: 6, tag_id: 3)
Represent.create!(course_id: 7, tag_id: 8)
Represent.create!(course_id: 7, tag_id: 9)
Represent.create!(course_id: 7, tag_id: 10)

# MAT e INFO #
Represent.create!(course_id: 8, tag_id: 12)
Represent.create!(course_id: 8, tag_id: 13)
Represent.create!(course_id: 9, tag_id: 3)
Represent.create!(course_id: 9, tag_id: 14)
Represent.create!(course_id: 10, tag_id: 12)
Represent.create!(course_id: 10, tag_id: 13)

# MED FARM E PREV #
Represent.create!(course_id: 11, tag_id: 15)
Represent.create!(course_id: 11, tag_id: 18)
Represent.create!(course_id: 12, tag_id: 16)
Represent.create!(course_id: 12, tag_id: 17)
Represent.create!(course_id: 12, tag_id: 18)
Represent.create!(course_id: 13, tag_id: 19)
Represent.create!(course_id: 14, tag_id: 15)
Represent.create!(course_id: 14, tag_id: 20)
Represent.create!(course_id: 15, tag_id: 21)
Represent.create!(course_id: 15, tag_id: 17)
Represent.create!(course_id: 15, tag_id: 23)
Represent.create!(course_id: 15, tag_id: 18)
Represent.create!(course_id: 15, tag_id: 22)
Represent.create!(course_id: 16, tag_id: 24)
Represent.create!(course_id: 16, tag_id: 25)
Represent.create!(course_id: 16, tag_id: 18)
Represent.create!(course_id: 16, tag_id: 17)
Represent.create!(course_id: 17, tag_id: 18)
Represent.create!(course_id: 17, tag_id: 17)

# FISICA #
Represent.create!(course_id: 18, tag_id: 26)
Represent.create!(course_id: 18, tag_id: 27)
Represent.create!(course_id: 18, tag_id: 28)
Represent.create!(course_id: 19, tag_id: 26)
Represent.create!(course_id: 19, tag_id: 27)
Represent.create!(course_id: 19, tag_id: 28)

# ARCHITETTURA #
Represent.create!(course_id: 20, tag_id: 4)
Represent.create!(course_id: 20, tag_id: 6)

# ECONOMIA #
Represent.create!(course_id: 21, tag_id: 31)
Represent.create!(course_id: 21, tag_id: 32)
Represent.create!(course_id: 22, tag_id: 31)
Represent.create!(course_id: 22, tag_id: 32)
Represent.create!(course_id: 22, tag_id: 30)
Represent.create!(course_id: 22, tag_id: 29)

# GIURISPRUDENZA #
Represent.create!(course_id: 23, tag_id: 33)
Represent.create!(course_id: 23, tag_id: 34)
Represent.create!(course_id: 23, tag_id: 35)
Represent.create!(course_id: 24, tag_id: 34)
Represent.create!(course_id: 24, tag_id: 35)

# BIOTECH #
Represent.create!(course_id: 25, tag_id: 36)
Represent.create!(course_id: 25, tag_id: 15)

# 25.times do |index|
#   Represent.create!(
#                 course_id: Faker::Number.between(1,100),
#                 tag_id: Faker::Number.between(1,100))
# end

p "Created #{Represent.count} Represents"
