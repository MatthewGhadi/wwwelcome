Tag.destroy_all

Tag.create!(name: "software", tag_type: "C")
Tag.create!(name: "electronics", tag_type: "C")
Tag.create!(name: "programming", tag_type: "C")
Tag.create!(name: "architecture", tag_type: "C")
Tag.create!(name: "dam", tag_type: "C")
Tag.create!(name: "building", tag_type: "C")
Tag.create!(name: "bridges", tag_type: "C")
Tag.create!(name: "cars", tag_type: "C")
Tag.create!(name: "motorbikes", tag_type: "C")
Tag.create!(name: "industry", tag_type: "C")
Tag.create!(name: "raspberry", tag_type: "C")
Tag.create!(name: "number", tag_type: "C")
Tag.create!(name: "maths", tag_type: "C")
Tag.create!(name: "computer", tag_type: "C")
Tag.create!(name: "chemistry", tag_type: "C")
Tag.create!(name: "medicine", tag_type: "C")
Tag.create!(name: "patient", tag_type: "C")
Tag.create!(name: "disease", tag_type: "C")
Tag.create!(name: "teeth", tag_type: "C")
Tag.create!(name: "laboratory", tag_type: "C")
Tag.create!(name: "voice", tag_type: "C")
Tag.create!(name: "children", tag_type: "C")
Tag.create!(name: "sing", tag_type: "C")
Tag.create!(name: "legs", tag_type: "C")
Tag.create!(name: "back", tag_type: "C")
Tag.create!(name: "physics", tag_type: "C")
Tag.create!(name: "space", tag_type: "C")
Tag.create!(name: "atom", tag_type: "C")
Tag.create!(name: "markets", tag_type: "C")
Tag.create!(name: "stocks", tag_type: "C")
Tag.create!(name: "business", tag_type: "C")
Tag.create!(name: "management", tag_type: "C")
Tag.create!(name: "lawyer", tag_type: "C")
Tag.create!(name: "justice", tag_type: "C")
Tag.create!(name: "law", tag_type: "C")
Tag.create!(name: "science", tag_type: "C")


#  100.times do |index|
#    Tag.create!(
#                  name: "" + Faker::Lorem.word,
#                  tag_type: Faker::String.random(1))
#  end

p "Created #{Tag.count} Tags"