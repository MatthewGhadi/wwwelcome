# -> https://davidmles.com/seeding-database-rails/
Course.destroy_all

# INGEGNERIA #
Course.create!(
  name: "Ingegneria Civile e Ambientale",
  course_class: "L-7",
  site: "http://www.unife.it/ing/civile",
  access: "Libero",
  students: 150,
  duration: 3,
  department_id: 1,
  description: "Il corso di studi in ingegneria civile e ambientale mira a fornire una solida preparazione nelle materie di base dell'ingegneria civile, ponendo attenzione agli aspetti ambientali.")

Course.create!(
  name: "Ingegneria Elettronica e Informatica",
  course_class: "L-8",
  site: "http://www.unife.it/ing/informazione",
  access: "Libero",
  students: 300,
  duration: 3,
  department_id: 1,
  description: "Il corso di studio fornisce conoscenze e competenze tecnologiche altamente qualificanti per i settori dell'Ingegneria Elettronica e dell'Ingegneria Informatica, e ampiamente richieste dal mondo del lavoro.")

Course.create!(
  name: "Ingegneria Meccanica",
  course_class: "L-9",
  site: "http://www.unife.it/ing/meccanica",
  access: "Libero",
  students: 250,
  duration: 3,
  department_id: 1,
  description: "Il corso di Laurea in Ingegneria Meccanica offerto dal Dipartimento di Ingegneria dell'Università di Ferrara è ad accesso libero, con un percorso formativo organizzato in tre anni di studio.")
  
Course.create!(
  name: "Ingegneria Civile",
  course_class: "LM-23",
  site: "http://www.unife.it/ing/lm.civile",
  access: "Libero",
  students: 50,
  duration: 2,
  department_id: 1,
  description: "Il percorso formativo della laurea magistrale in Ingegneria civile è articolato su due indirizzi.Il primo (Costruzioni) è fortemente incentrato sulle materie riguardanti le costruzioni, con particolare attenzione alla componente sismica, la geotecnica, la cantieristica e l'edilizia, non trascurando gli aspetti di impatto ambientale delle opere e la progettazione territoriale.il secondo (Ambientale) si concentra invece su materie quali l'idraulica e le costruzioni idrauliche, le infrastrutture viarie e le opere in terra, approfondendo altresì aspetti legati alla bonifica dei siti inquinati, il trattamento dei rifiuti solidi, lo sfruttamento di fonti energetiche rinnovabili, il trattamento delle acque reflue e il monitoraggio.")

Course.create!(
  name: "Ingegneria Elettronica per l'ICT",
  course_class: "LM-29",
  site: "http://www.unife.it/ing/lm.tlcele",
  access: "Libero",
  students: 30,
  duration: 2,
  department_id: 1,
  description: "Il corso di studio vuole formare la figura di un Ingegnere Elettronico con competenze interdisciplinari su tecnologie, dispositivi, sistemi, architetture e reti in grado di affrontare con efficacia le sfide poste dai nuovi scenari applicativi dell' ICT")

  
Course.create!(
  name: "Ingegneria Informatica e dell'Automazione",
  course_class: "LM-32",
  site: "http://www.unife.it/ing/lm.infoauto",
  access: "Libero",
  students: 100,
  duration: 2,
  department_id: 1,
  description: "Il corso di Laurea Magistrale in Ingegneria Informatica e dell'Automazione, offerto dal Dipartimento di Ingegneria dell'Università di Ferrara, è un percorso formativo che fornisce conoscenze e competenze tecnologiche altamente qualificanti e ampiamente richieste dal mondo del lavoro nell'area ICT (Information and Communication Technology) dell'industria e dei servizi.")

Course.create!(
  name: "Ingegneria Meccanica",
  course_class: "LM-33",
  site: "http://www.unife.it/ing/lm.meccanica",
  access: "Libero",
  students: 150,
  duration: 2,
  department_id: 1,
  description: "Le attività didattiche del corso di laurea si svolgono presso il Polo Scientifico-Tecnologico dell'Università di Ferrara, un campus con un ambiente di studio piacevole, servizi efficienti e ampi spazi per lo studio a disposizione degli studenti.")

# MAT e INFO #
Course.create!(
name: "Matematica",
course_class: "L-35",
site: "http://www.unife.it/scienze/matematica",
access: "Libero",
students: 30,
duration: 3,
department_id: 2,
description: "Il Corso di studio consente l’ingresso immediato nel mondo del lavoro, oltre a dare la possibilità di proseguire gli studi. Il laureato è in grado di svolgere in autonomia compiti tecnici e professionali, di supporto modellistico-matematico e computazionale ad attività dell’industria, della finanza, dei servizi e nella pubblica amministrazione.")
#
Course.create!(
name: "Informatica",
course_class: "L-31",
site: "http://www.unife.it/scienze/informatica",
access: "Libero",
students: 150,
duration: 3,
department_id: 2,
description: "Il corso di laurea in Informatica forma professionisti in grado di progettare, sviluppare e gestire sistemi e reti informatiche, mediante le tecnologie e metodologie più avanzate e di sviluppare applicazioni e servizi basati sul loro utilizzo.")
#
Course.create!(
  name: "Matematica",
  course_class: "LM-40",
  site: "http://www.unife.it/scienze/lm.matematica",
  access: "Libero",
  students: 20,
  duration: 2,
  department_id: 2,
  description: "Le attività didattiche del corso di laurea si svolgono presso il Polo Scientifico-Tecnologico dell'Università di Ferrara, un campus con un ambiente di studio piacevole, servizi efficienti e ampi spazi per lo studio a disposizione degli studenti.")
#

# MED FARM E PREV #
Course.create!(
  name: "Farmacia",
  course_class: "LM-13",
  site: "http://www.unife.it/farmacia/lm.farmacia",
  access: "Test d'ingresso",
  students: 600,
  duration: 5,
  department_id: 3,
  description: "L’aspetto caratterizzante il corso di studio in Farmacia dell’Università degli Studi di Ferrara è la preparazione che lo studente viene ad acquisire, rispetto non solo al farmaco tradizionalmente inteso e alla sua evoluzione, ma anche in un’ottica dei prodotti della salute completa.")
#
Course.create!(
  name: "Medicina e Chirurgia",
  course_class: "LM-41",
  site: "http://www.unife.it/medicina/lm.medicina",
  access: "Accesso Programmato",
  students: 600,
  duration: 6,
  department_id: 3,
  description: "Il Corso di Studio Laurea Magistrale a ciclo unico in Medicina e Chirurgia  è a numero programmato a livello nazionale, pertanto l'immatricolazione  è subordinata al superamento di un test di ammissione predisposto dal MIUR.")
#
Course.create!(
  name: "Odontoiatria e Protesi Dentaria",
  course_class: "LM-46",
  site: "http://www.unife.it/medicina/lm.odontoiatria",
  access: "Accesso Programmato",
  students: 200,
  duration: 6,
  department_id: 3,
  description: "Il Corso di Laurea Magistrale  in Odontoiatria e Protesi Dentaria in Italia ha una durata di sei anni. L'accesso al corso di Studio è programmato a livello Nazionale (numero chiuso). ")
#
Course.create!(
  name: "Chimica",
  course_class: "L-27",
  site: "http://www.unife.it/scienze/chimica",
  access: "Libero",
  students: 500,
  duration: 3,
  department_id: 3,
  description: "La Chimica è una disciplina in continua evoluzione ed occupa un ruolo centrale nella vita dell'uomo, nell'ambiente naturale e nella società moderna. ")
#
Course.create!(
  name: "Logopedia",
  course_class: "L/SNT2",
  site: "http://www.unife.it/medicina/logopedia",
  access: "Accesso Programmato",
  students: 75,
  duration: 3,
  department_id: 3,
  description: "Il corso si pone l'obiettivo primario di formare logopedisti che abbiano conoscenze e competenze tecnico-pratiche e comportamentali adeguate allo svolgimento della migliore pratica professionale secondo quanto previsto dal profilo professionale e dal core curriculum del logopedista.")
#
Course.create!(
  name: "Fisioterapia",
  course_class: "L/SNT2",
  site: "http://www.unife.it/medicina/fisioterapia",
  access: "Accesso Programmato",
  students: 90,
  duration: 3,
  department_id: 3,
  description: "Lo scopo di questo corso di laurea è di preparare fisioterapisti laureati che siano in grado di svolgere un'attività professionale incentrata sulla persona, etica, efficace e sicura, e di assumere piena responsabilità personale e professionale delle loro azioni.")
#
Course.create!(
  name: "Infermieristica",
  course_class: "L/SNT1",
  site: "http://www.unife.it/medicina/infermieristica",
  access: "Accesso Programmato",
  students: 450,
  duration: 3,
  department_id: 3,
  description: "Una laurea sanitaria che coniuga le conoscenze del sapere teorico al tirocinio pratico nelle varie sedi assistenziali, con l’obiettivo di formare un professionista dalle elevate competenze, in grado di affrontare e svolgere un fondamentale servizio  alla persona ed alla collettività.")
#
# FISICA #
Course.create!(
  name: "Fisica",
  course_class: "L-30",
  site: "http://www.unife.it/scienze/fisica",
  access: "Libero",
  students: 60,
  duration: 3,
  department_id: 4,
  description: "Il Corso di Laurea Triennale in Fisica si propone di fornire agli studenti conoscenze relative alla fisica classica e moderna, fornendo competenze sia teoriche che sperimentali e di laboratorio. ")
#
Course.create!(
  name: "Fisica",
  course_class: "LM-17",
  site: "http://www.unife.it/scienze/lm.physics/",
  access: "libero",
  students: 25,
  duration: 2,
  department_id: 4,
  description:  " This is an opportunity for students to acquire an in-depth knowledge of modern physics, both theoretical and experimental, with a strong focus both on basic science and advanced applied physics; students will have an opprotunity to study and work in close contact with leading-edge research groups.")
#

# ARCHITETTURA #
Course.create!(
  name: "Architettura",
  course_class: "LM-4",
  site: "http://www.unife.it/architettura/lm.architettura",
  access: "Accesso Programmato",
  students: 450,
  duration: 5,
  department_id: 5,
  description: "Il Corso di studio in Architettura, allineato alla direttiva europee, intende fornire, allo studente tutti gli strumenti per un approccio consapevole alla professione: formare una figura di architetto capace di gestire l'ampiezza e la complessità delle conoscenze di carattere pluridisciplinare utili e necessarie per progettare e gestire l'ambiente e i processi di antropizzazione.")
#

# ECONOMIA #
Course.create!(
  name: "Scienze dell'economia e gestione aziendale",
  course_class: "L-18",
  site: "http://www.unife.it/economia/economia",
  access: "Test d'ingresso",
  students: 600,
  duration: 3,
  department_id: 6,
  description: "Il Corso di Laurea in Economia tratta molteplici tematiche in ambitoeconomico, aziendale, giuridico, offrendoti una preparazione solida e trasversale, utile per intraprendere svariati percorsi professionali, all’interno di imprese, banche e istituzioni finanziarie, amministrazioni pubbliche e istituzioni internazionali.")
#
Course.create!(
  name: "Economia, Mercati e Management",
  course_class: "LM-77",
  site: "http://www.unife.it/economia/lm.economia",
  access: "Test d'ingresso",
  students: 200,
  duration: 2,
  department_id: 6,
  description: "Il Corso di Laurea in Economia tratta molteplici tematiche in ambitoeconomico, aziendale, giuridico, offrendoti una preparazione solida e trasversale, utile per intraprendere svariati percorsi professionali, all’interno di imprese, banche e istituzioni finanziarie, amministrazioni pubbliche e istituzioni internazionali.")
#

# GIURISPRUDENZA #

Course.create!(
  name: "Giurisprudenza",
  course_class: "LMG-01",
  site: "http://www.unife.it/giurisprudenza/giurisprudenza/",
  access: "Libero",
  students: 800,
  duration: 5,
  department_id: 7,
  description: "Il Corso di Laurea Magistrale a ciclo unico in Giurisprudenza prevede un percorso di cinque anni, per un totale di 300 crediti e 32 esami. Assicura una preparazione completa in tutti i rami del diritto, con adeguata attenzione anche alle radici storiche e culturali del nostro sistema giuridico. ")
#
Course.create!(
  name: "Operatore dei servizi giuridici",
  course_class: "L-14",
  site: "http://www.unife.it/giurisprudenza/operatore-servizi-giuridici",
  access: "Libero",
  students: 250,
  duration: 3,
  department_id: 7,
  description: "Il Corso di Laurea triennale in Operatore dei servizi giuridici risulta articolato su un percorso formativo di tre anni, per un totale di 180 crediti, strutturati essenzialmente in due parti: nei primi due anni, sono stati collocati tutti gli insegnamenti obbligatori, che mirano a fornire allo studente le coordinate storiche, culturali e teoretiche essenziali nell'esperienza giuridica italiana, le nozioni di base della lingua inglese giuridica e soprattutto la conoscenza approfondita dei settori fondamentali del diritto italiano e dell'Unione europea.")
#

# BIOTECH #

Course.create!(
  name: "Biotecnologie",
  course_class: "L-2",
  site: "http://www.unife.it/sveb/biotecnologie",
  access: "Libero",
  students: 3000,
  duration: 3,
  department_id: 8,
  description: "La laurea triennale in Biotecnologie dell'Università di Ferrara è un corso di laurea con ingresso libero a partire dall'a.a. 2017-2018. Si prefigge di formare figure professionali che posseggano una adeguata preparazione culturale nell'ambito della biologia di base ed applicata anche ai sistemi ambientali, delle tecnologie ricombinanti, delle tecniche di coltura in-vitro e di produzione di metaboliti mediante fermentazione o utilizzo di enzimi isolati.")
#


# 100.times do |index|
#   Course.create!(
#                 name: Faker::Educator.course,
#                 course_class: "LM" + Faker::Number.number(2),
#                 site: "http://www.unife.it/" + Faker::String.random(3...100),
#                 access: Faker::String.random(3...40),
#                 students: Faker::Number.positive,
#                 duration: Faker::Number.positive,
#                 department_id: Faker::Number.between(1,100),
#                 description: Faker::Lorem.paragraph)
# end

p "Created #{Course.count} Courses"
