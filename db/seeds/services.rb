Service.destroy_all


Service.create!(
                name: "Banca Intesa San Paolo",
                office: "Corso Porta Reno 44, 44121 Ferrara, Italia",
                site: "www.bancaintesa.it",
                latitude: "44.8346",
                longitude: "11.6184")

Service.create!(
                name: "Unità di Supporto Didattica e AQ",
                office: "Via Giuseppe Saragat 1, Blocco B terzo piano, 44124 Ferrara, Italia",
                site: "http://www.unife.it/studenti/tutorato-didattico/contatti-e-orari-di-apertura",
                latitude: "44.8338",
                longitude: "11.5994")

Service.create!(
                name: "Mensa Tecno-Polo",
                office: "Via Giuseppe Saragat 2, 44124 Ferrara, Italia",
                site: "http://www.unife.it/studenti/tutorato-didattico/contatti-e-orari-di-apertura",
                latitude: "44.8357",
                longitude: "11.5987")

Service.create!(
                name: "Biblioteca Ariostea",
                office: "Via delle Scienze 17, 44124 Ferrara, Italia",
                site: "http://archibiblio.comune.fe.it/271/biblioteca-comunale-ariostea",
                latitude: "44.8330",
                longitude: "11.6217")

Service.create!(
                name: "CTS, Centro Turistico Studentesco",
                office: "Via Cairoli 35, 44124 Ferrara, Italia",
                site: "https://www.cts.it",
                latitude: "44.8361",
                longitude: "11.6218")

Service.create!(
                name: "Arcispedale Sant'Anna",
                office: "Via Aldo Moro 8, 44124 Cona, Ferrara, Italia",
                site: "http://www.ospfe.it",
                latitude: "44.7992",
                longitude: "11.6945")

Service.create!(
                name: "Er.Go - Azienda Regionale Per Il Diritto Agli Studi Superiori",
                office: "Via Guido D'Arezzo 2, 44121 Ferrara, Italia",
                site: "http://www.er-go.it/",
                latitude: "44.8396063",
                longitude: "11.6340909")

Service.create!(
                name: "Aula Studio Di Medicina",
                office: "Via Mortara 96a, 44121 Ferrara, Italia",
                site: nil,
                latitude: "44.8404182",
                longitude: "11.6329916")

Service.create!(
                name: "Ufficio Postale Poste Italiane",
                office: "Via Mortara 287, 44121 Ferrara, Italia",
                site: "https://www.poste.it/",
                latitude: "44.8405328",
                longitude: "11.6327563")
                
Service.create!(
                name: "Farmacia Comunale 1 'Porta Mare'",
                office: "Corso Porta Mare 114, 44121 Ferrara, Italia",
                site: "https://www.afm.fe.it/",
                latitude: "44.8406601",
                longitude: "11.6344275") 
                                
Service.create!(
                name: "Camplus Apartments Ferrara - Residenza Darsena",
                office: "Via Darsena 81/a, 44122 Ferrara, Italia",
                site: "https://www.camplusapartments.it/studenti/offerta-abitativa/ferrara/residenza-darsena/9/",
                latitude: "44.837012",
                longitude: "11.6014269")
                                                
Service.create!(
                name: "UCI Cinemas Ferrara",
                office: "Via Darsena 73, 44100 Ferrara, Italia",
                site: "https://www.ucicinemas.it/cinema/emilia-romagna/ferrara/uci-cinemas-ferrara/",
                latitude: "44.8366107",
                longitude: "11.6021151")
                                                                
Service.create!(
                name: "Biblioteca Chimico-Biologica di Santa Maria delle Grazie",
                office: "Via Fossato di Mortara 15, 44121 Ferrara, Italia",
                site: "http://sba.unife.it/it",
                latitude: "44.8375628",
                longitude: "11.6312786")
                                                                                
Service.create!(
                name: "Camplus Apartments Ferrara - Ufficio Commerciale",
                office: "Via Mortara 171, 44121 Ferrara, Italia",
                site: "https://www.camplusapartments.it/studenti/offerta-abitativa/ferrara/",
                latitude: "44.8374179",
                longitude: "11.6311676")
                                                                                                
Service.create!(
                name: "Centro per l'impiego",
                office: "Via Fossato di Mortara 78, 44121 Ferrara, Italia",
                site: "https://www.agenzialavoro.emr.it/ferrara",
                latitude: "44.8358761",
                longitude: "11.6338434")
                                                                                                               
Service.create!(
                name: "Genetica Medica",
                office: "Via Fossato di Mortara, 49, 44121 Ferrara, Italia",
                site: "http://www.ospfe.it/reparti-e-servizi/reparti-dalla-a-alla-m-1/genetica-medica",
                latitude: "44.8354276",
                longitude: "11.6335325")
                                                                                                                               
Service.create!(
                name: "Centre Haemostasis & Thrombosis",
                office: "Cittadella della Salute, Via Rampari di San Rocco 27, 44121 Ferrara, Italia",
                site: nil,
                latitude: "44.8349682",
                longitude: "11.6328789") 
                                                                                                                                              
Service.create!(
                name: "Casa della Salute 'Cittadella S. Rocco'",
                office: "Corso della Giovecca 203, 44121 Ferrara, Italia",
                site: "http://www.ausl.fe.it/home-page/news/archivio-news/2014/visita-guidata-alla-casa-della-salute-cittadella-s.-rocco#null",
                latitude: "44.8328557",
                longitude: "11.6314614")
                                                                                                                                                              
Service.create!(
                name: "Nuovo Body Club",
                office: "Via XX Settembre 47, 44121 Ferrara, Italia",
                site: "http://www.nuovobodyclub.it/",
                latitude: "44.8290097",
                longitude: "11.6232689") 
                                                                                                                                                                              
Service.create!(
                name: "Archicenter",
                office: "Via Cammello 74, 44121 Ferrara, Italia",
                site: "http://www.archicenter.net/",
                latitude: "44.8296999",
                longitude: "11.6217032")
                                                                                                                                                                                              
Service.create!(
                name: "Pam Local",
                office: "Via Bersaglieri del Po 66, 44121 Ferrara, Italia",
                site: nil,
                latitude: "44.8356322",
                longitude: "11.621738")
                                                                                                                                                                                                              
Service.create!(
                name: "Copisteria Arianuova Di Barattini Manuela",
                office: "Via Arianuova 7, 44121 Ferrara, Italia",
                site: "https://www.facebook.com/groups/copisteriaarianuova/",
                latitude: "44.8447763",
                longitude: "11.6214322")
                                                                                                                                                                                                                              
Service.create!(
                name: "Cartolibreria Universitaria Arianuova",
                office: "Via Arianuova 3/A/B, 44121 Ferrara, Italia",
                site: nil,
                latitude: "44.8446689",
                longitude: "11.621665")
                                                                                                                                                                                                                                              
Service.create!(
                name: "Parcheggio Diamanti",
                office: "Via Arianuova 25, 44121 Ferrara, Italia",
                site: "http://www.ferraratua.com/",
                latitude: "44.8448409",
                longitude: "11.6205369")                              

# 100.times do |index|
#   Service.create!(
#                 name: Faker::Job.field,
#                 office: Faker::Address.full_address,
#                 site: Faker::Internet.url,
#                 latitude: Faker::Address.latitude,
#                 longitude: Faker::Address.longitude)
# end

p "Created #{Service.count} Services"