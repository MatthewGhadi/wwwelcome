Tutorial.destroy_all

Tutorial.create!(
                title: "CFU",
                description: "Qualsiasi attività formativa (insegnamento, laboratorio, tirocinio o tesi) equivale ad un certo numero di crediti formativi.
                              Il credito formativo universitario (CFU) è impiegato per quantificare il lavoro di apprendimento dello studente e si calcola tenendo in considerazione sia le ore di attività didattiche in aula, che le ore di studio individuale richieste ad uno studente in possesso di un’adeguata preparazione iniziale, per acquisire conoscenze e abilità nelle attività formative previste dal piano degli studi.
                              A 1 CFU corrispondono complessivamente 25 ore di lavoro.
                              Il rapporto tra il numero di ore attribuite all’attività didattica in aula sulle 25 ore totali di un credito formativo universitario è diverso per ciascun corso di studio.
                              Tutti gli studenti che superano un esame, per es., di 12 crediti, maturano 12 crediti.
                              Può invece essere diversa la valutazione conseguita, che può variare da studente a studente, da 18/30 a 30/30 e lode.
                              Quindi, superando lo stesso esame, tutti gli studenti maturano lo stesso numero di crediti, ma possono conseguire valutazioni diverse.",
                site: "http://www.unife.it/orientamento/it/guida-matricole/prima-di-scegliere/cos2019e-un-credito-formativo-universitario")

Tutorial.create!(
                title: "TriennaleVSCiclounico",
                description: "I corsi di studio universitari cui è possibile accedere dopo il diploma possono essere le lauree triennali (della durata di 3 anni) e quelle magistrali a ciclo unico (della durata di cinque o sei anni).
                              Alle lauree magistrali di durata biennale (che sono corsi di II ciclo), si può accedere invece solo dopo aver conseguito la laurea triennale (di I ciclo).",
                site: "http://www.unife.it/orientamento/it/guida-matricole/prima-di-scegliere/qual-e-la-differenza-tra-un-corso-di-studio-triennale-e-uno-a-ciclo-unico")

Tutorial.create!(
                title: "NumeroProgrammatoVSAccessoLibero",
                description: "I corsi di studio ad accesso programmato sono quelli in cui l’iscrizione è possibile solo in seguito al superamento di un test di ammissione.
                              L’immatricolazione ai corsi di studio ad accesso libero non prevede invece il superamento di un test di selezione ma avviene in maniera automatica, dopo aver completato le procedure di immatricolazione.",
                site: "http://www.unife.it/orientamento/it/guida-matricole/prima-di-scegliere/qual-e-la-differenza-tra-un-corso-di-studio-a-numero-programmato-e-uno-ad-accesso-libero")

Tutorial.create!(
                title: "RequisitiDiAccesso",
                description: "A seguito dell’entrata in vigore della Riforma Universitaria (DM 270/2004) l’accesso all’Università, per le lauree triennali e per le lauree a ciclo unico, prevede una verifica obbligatoria delle conoscenze iniziali dello studente.
                              La finalità del test è di verificare il possesso dei requisiti di conoscenze iniziali richiesti dallo specifico corso di studio.
                              Qualora il test evidenziasse delle lacune, potrebbero venire attribuiti obblighi formativi aggiuntivi (O.F.A) da assolvere nel primo anno di corso.
                              Il test di verifica delle conoscenze iniziali non deve essere confuso con la prova di ammissione ai corsi di studio a numero programmato poiché l'esito del test di verifica delle conoscenze iniziali non preclude in alcun caso l'iscrizione al corso di studio prescelto.",
                site: "http://www.unife.it/orientamento/it/guida-matricole/prima-di-scegliere/come-posso-conoscere-i-requisiti-di-accesso-richiesti-dai-corsi-di-studio")

Tutorial.create!(
                title: "StudiareAllEstero",
                description: "Se ti interessa affrontare un’esperienza di studio o tirocinio al di fuori dei nostri confini, l’Università di Ferrara ti offre la possibilità di partecipare a diversi programmi di mobilità internazionale, tra cui il programma Erasmus+, Erasmus+ Traineeship e il progetto Atlante.
                              Con il Programma Erasmus+ è possibile partecipare alla selezione per trascorrere un periodo di tempo compreso tra 3 e 12 mesi presso uno dei 27 Paesi dell’Unione Europea, oltre ad Islanda, Liechtenstein, Norvegia, Turchia, Croazia e Svizzera, durante il quale potrai effettuare attività di studio o di tirocinio che il tuo corso di studio avrà precedentemente approvato.
                              Potrai così venire a contatto con persone e culture diverse, ambienti accademici differenti e avrai modo di acquisire una buona conoscenza della lingua del Paese ospitante.",
                site: "http://www.unife.it/orientamento/it/guida-matricole/prima-di-scegliere/e2019-possibile-trascorrere-un-periodo-di-studio-all2019estero")
                
Tutorial.create!(
                title: "UniFEOrienta",
                description: "L’Ufficio Orientamento in entrata organizza, annualmente, UniFE ORIENTA, manifestazione generale di orientamento ad una scelta consapevole, rivolto agli Studenti delle Scuole superiori, ai Genitori ed ai Referenti di orientamento in uscita.
                              Il 14 e 15 febbraio 2018 presso gli stand dei singoli Corsi di studio, Docenti Universitari, Manager didattici e studenti già iscritti all’Università saranno a disposizione per fornire informazioni dettagliate sui percorsi di formazione, sulle modalità di verifica delle conoscenze iniziali o sulle prove di ammissione, sull'organizzazione didattica e sui servizi di supporto agli studenti. Durante le due giornate i Docenti responsabili si alterneranno nelle varie aule per illustrare ai partecipanti il dettaglio dei propri corsi di studio
                              Accanto agli stand di Dipartimento verranno allestiti spazi espositivi dove gli Studenti potranno trovare le informazioni sui servizi e agevolazioni che Enti ed Istituzioni pubbliche ed Associazioni mettono a disposizione delle future matricole per una sinergia fra città e Università.",
                site: "http://www.unife.it/orientamento/it/perche-unife/servizi-di-orientamento")
        
Tutorial.create!(
                title: "GuidaPerLeMatricole",
                description: "La Guida per matricole contiene le risposte alle domande più frequenti da parte di chi si avvicina all'Università.",
                site: "http://www.unife.it/orientamento/it/guida-matricole/guida-matricole")

Tutorial.create!(
                title: "UniversItaly",
                description: "Per avere una panoramica di tutti i Corsi di studio a livello nazionale, è possibile consultare UniversItaly, il portale del Ministero dell'Istruzione, dell'Università e della Ricerca, creato appositamente per accompagnare gli studenti nel loro percorso di studi. Consente di  conoscere i Corsi di studio di tutte le Università in Italia, inclusi quelli in lingua inglese, le modalità di accesso, i costi previsti e molto altro ancora. Sul portale è pubblicata, per ciascun Corso di Studio, la Scheda Unica Annuale (SUA-CdS)",
                site: "https://www.universitaly.it/")

Tutorial.create!(
                title: "AlmaOrièntati",
                description: "Uno strumento utile ed efficace per la scelta consapevole del Corso di studio, realizzato dal Consorzio Interuniversitario AlmaLaurea, è AlmaOrièntati che consiste in un percorso di orientamento che si articola in quattro sezioni, al termine del quale lo studente riceve il proprio profilo personalizzato.",
                site: "http://www.almaorientati.it/orienta/intro.aspx")

Tutorial.create!(
                title: "ServiziDiAccoglienza",
                description: "L'Università di Ferrara mette a disposizione numerosi servizi di accoglienza.

                              Servizio di Accoglienza Matricole 
                              Ogni anno, nel mese di settembre si svolge un servizio di accoglienza rivolto agli immatricolati al primo anno di una laurea o di una laurea magistrale a ciclo unico, ed è finalizzato a favorire l'approccio dei nuovi studenti con il contesto universitario.

                              Management Didattico
                              I Manager didattici dei corsi di studio forniscono, in seguito all’immatricolazione,  informazioni sull’organizzazione della didattica e  sui servizi di supporto alla didattica di ogni corso di studio sia attraverso il servizio di ascolto, sia  attraverso incontri di accoglienza  matricole appositamente organizzati in concomitanza dell’inizio delle lezioni.

                              Tutorato Internazionale
                              Gli studenti internazionali iscritti all'Università di Ferrara e gli studenti Erasmus in ingresso e in uscita possono usufruire del servizio di Tutorato Internazionale che consiste in un servizio di accoglienza svolto da studenti già iscritti a UniFE.",
                site: "http://www.unife.it/orientamento/it/perche-unife/servizi-di-accoglienza")

Tutorial.create!(
                title: "AccoglienzaDisabili",
                description: "Accoglienza studentesse e studenti con disabilità o DSA.",
                site: "http://www.unife.it/studenti/disabilita-dsa")

Tutorial.create!(
                title: "Biblioteche",
                description: "Le biblioteche dell'Ateneo, inserite nel Polo Bibliotecario Ferrarese e facenti capo al Sistema Bibliotecario di Ateneo, sono organizzate in macroaree.
                              Informazioni dettagliate sulle singole biblioteche, indirizzi, orari, etc, sono consultabili nella pagina web dedicata alle biblioteche Unife",
                site: "http://sba.unife.it/it/biblioteche")

Tutorial.create!(
                title: "SaleStudio",
                description: "Oltre alle sale di lettura presenti nelle biblioteche, sono disponibili in Ateneo altre sale studio collocate presso Dipartimenti, Poli o in strutture convenzionate. Unife offre inoltre agli studenti dell’Ateneo l’accesso ad alcune sale studio la domenica e in orari serali",
                site: "http://sba.unife.it/it/sale-studio/sale-lettura")

Tutorial.create!(
                title: "ManagerDidattici",
                description: "Il Manager Didattico è il punto di riferimento per tutti gli aspetti che riguardano l’organizzazione della didattica dei corsi di studio.
                              Ogni corso di studio ha un Manager Didattico di riferimento.",
                site: "http://www.unife.it/studenti/manager-didattici")
              
Tutorial.create!(
                title: "SegreterieStudenti",
                description: "La sala di attesa agli sportelli degli Uffici carriera e Ingresso-Incoming che si trova piano terra apre 30 minuti prima rispetto all'orario di accesso al pubblico
                              L'App Qurami fornisce un biglietto già due ore prima dell'apertura al pubblico degli sportelli; la sala d'attesa , dove è posizionato l'erogatore dei biglietti, apre mezz'ora prima dell'accesso al pubblico agli sportelli. Quindi, se usi l'App da casa prima dell'apertura della sala,  sei certo di avere la precedenza. Usa Qurami, evita la coda
                              Responsabile della Ripartizione Segreterie Studentesse e Studenti e Diritto allo Studio: Lavinia Cavallini",
                site: "http://www.unife.it/studenti/offerta-formativa/s-s/segreterie-studenti")

Tutorial.create!(
                title: "AccessibilitàEdifici",
                description: "All'interno di questa sezione del sito, dedicata al monitoraggio degli edifici dell'Ateneo, è possibile scaricare e consultare le piante dei locali contenenti indicazioni relative all'accessibilità da parte di persone con disabilità. Nella mappa sottostante è riportata la localizzazione delle strutture di Unife fin qui mappate all'interno della Città e contrassegnate da un puntatore di colore diverso (il colore definisce il livello di accessibilità delle strutture. Verde significa accessibile, giallo semi accessibile, rosso non accessibile). Cliccando sui puntatori o selezionando la struttura interessata dall'elenco delle strutture che si trova nella colonna a destra della pagina, sarà possibile accedere alle informazioni relative agli edifici di riferimento (indirizzo, livello di accessibilità, planimetrie dotate di testo alternativo).",
                site: "http://www.unife.it/studenti/disabilita-dsa/accessibilita-edifici/")

Tutorial.create!(
                title: "PostaElettronica",
                description: "Servizio di posta UNIFE",
                site: "https://sso.student.unife.it/simplesaml/module.php/core/loginuserpass.php?AuthState=_5bf08fb057f122ce1a65d16384eae530165c5691f9%3Ahttps%3A%2F%2Fsso.student.unife.it%2Fsimplesaml%2Fsaml2%2Fidp%2FSSOService.php%3Fspentityid%3Dgoogle.com%26cookieTime%3D1550918932%26RelayState%3Dhttps%253A%252F%252Fwww.google.com%252Fa%252Fstudent.unife.it%252FServiceLogin%253Fservice%253Dmail%2526passive%253Dtrue%2526rm%253Dfalse%2526continue%253Dhttps%25253A%25252F%25252Fmail.google.com%25252Fmail%25252F%2526ss%253D1%2526ltmpl%253Ddefault%2526ltmplcache%253D2%2526emr%253D1%2526osid%253D1#")

Tutorial.create!(
                title: "WIFE",
                description: "Con Wi-Fe (Wireless Internet uniFE) è possibile navigare in Internet ad alta velocità, collegandosi via radio, senza bisogno di cavi, in tutto l'Ateneo.",
                site: "http://www.unife.it/areainformatica/servizi/wife/wife")

Tutorial.create!(
                title: "VPN",
                description: "La VPN trasforma una connessione remota o esterna alla rete locale universitaria (ad esempio connessioni modem, ADSL commerciali, reti di altri enti, ecc.) in una connessione virtuale alla rete d’Ateneo, quindi con gli stessi permessi che si hanno all'interno dell'Università (vedere le note per ulteriori dettagli). Ciò consente di accedere alle risorse riservate e protette tipo le riviste on-line o le cartelle condivise nei PC dell'ufficio.",
                site: "http://www.unife.it/areainformatica/servizi/vpn")

Tutorial.create!(
                title: "RicercaContattoDocenti",
                description: "Tool per la ricerca dei contatti dei docenti UNIFE.",
                site: "http://servizi.unife.it/rubrica/")

Tutorial.create!(
                title: "SOSUnife",
                description: "Help Online SOS - Supporto Online Studentesse e studenti.",
                site: "https://php.unife.it/sos/")

Tutorial.create!(
                title: "Immatricolazione",
                description: "Immatricolazioni e iscrizioni a lauree e lauree magistrali.",
                site: "http://www.unife.it/studenti/immatricolazioni-e-iscrizioni")

Tutorial.create!(
                title: "Qurami",
                description: "L'App Qurami fornisce un biglietto già due ore prima dell'apertura al pubblico degli sportelli; la sala d'attesa , dove è posizionato l'erogatore dei biglietti, apre mezz'ora prima dell'accesso al pubblico agli sportelli. Quindi, se usi l'App da casa prima dell'apertura della sala, sei certo di avere la precedenza. Usa Qurami, evita la coda!",
                site: "https://app.qurami.com/")

Tutorial.create!(
                title: "AlmaLaurea",
                description: "AlmaLaurea è un servizio innovativo che rende disponibili online i curricula di studenti/studentesse, laureandi/e laureati/e, diplomati/e di master e dottori di ricerca, ponendosi come punto di incontro fra università e aziende.",
                site: "http://www.unife.it/studenti/servizi-per-te/almalaurea")

Tutorial.create!(
                title: "CentroLinguistico",
                description: "Nel 1998 presso l’Università degli studi di Ferrara nasce il Centro di ricerca e servizi denominato Centro Linguistico di Ateneo (C.L.A.).

                              Il CLA si configura come Centro universitario. Ogni anno più di 300 studenti frequentano i nostri corsi. Il CLA organizza corsi di italiano L2 per studenti stranieri in mobilità, studenti stranieri iscritti ai Corsi di Laurea, specializzazione, dottorato di ricerca, al Conservatorio G. Frescobaldi.

                              Il CLA è ​sede di Certificazione di Italiano Lingua Straniera CILS dell’ Università per stranieri di Siena. Sono previsti inoltre corsi di Inglese Accademico e di lingua Portoghese.

                              La nostra biblioteca-mediateca è aperta a tutte le studentesse e studenti e al personale di UNIFE.",
                site: "http://www.unife.it/centri/cla/it")

Tutorial.create!(
                title: "PerchèIscriversi",
                description: "Dieci boni motivi per iscriversi a UNIFE.",
                site: "http://www.unife.it/iscriversi")

Tutorial.create!(
                title: "CorsiDiStudio",
                description: "Unife offre Corsi di studio (lauree, lauree magistrali, lauree magistrali a ciclo unico) e una completa offerta formativa di Dottorati, Master, Corsi di formazione e perfezionamento, corsi per l'abilitazione all'insegnamento e Scuole di Specializzazione.",
                site: "http://www.unife.it/orientamento/it/studenti-scuole-superiori/i-corsi-di-studio-unife/cds-unife")

Tutorial.create!(
                title: "Sbocchi",
                description: "Verso il mondo del lavoro: sbocchi occupazionali e professionali dei corsi UniFE.",
                site: "http://www.unife.it/orientamento/it/studenti-scuole-superiori/i-corsi-di-studio-unife/sbocchi-occupazionali-e-professioni-dei-corsi-di-studio-unife")
                
Tutorial.create!(
                title: "PLS",
                description: "Il Piano Lauree Scientifiche (PLS), istituito a partire dal 2004 su iniziativa del MIUR (Ministero dell'Istruzione, Università e Ricerca), della Conferenza dei Presidi di Scienze e Tecnologie e di Confindustria, rappresenta una iniziativa consolidata finalizzata a favorire l'acquisizione, da parte degli studenti, di competenze scientifiche che rispondano alle attese del mondo del lavoro e alle esigenze della società contemporanea.",
                site: "http://www.unife.it/studenti/pls")
                                
Tutorial.create!(
                title: "OFA",
                description: "A  seguito della riforma D.M. 270/2004, l’accesso all’Università, per le lauree (di durata triennale) e le lauree magistrali a ciclo unico, deve essere preceduto da una verifica obbligatoria delle conoscenze iniziali dello studente con la possibilità di attribuzione di obblighi formativi aggiuntivi (OFA) da soddisfare nel primo anno di corso.",
                site: "http://www.unife.it/studenti/offerta-formativa/requisiti")
                


# 100.times do |index|
#   Tutorial.create!(
#                 title: Faker::Lorem.word,
#                 description: Faker::Lorem.paragraph)
# end

p "Created #{Tutorial.count} Tutorials"