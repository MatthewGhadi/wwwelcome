Department.destroy_all

Department.create!(
                name: "Dipartimento di Ingegneria",
                office: "Via Giuseppe Saragat 1, 44124 Ferrara, Italia",
                latitude: "44.8346474",
                longitude: "11.598298")

Department.create!(
                name: "Dipartimento di Matematica e Informatica",
                office: "Via Nicolò Machiavelli 30, 44121 Ferrara, Italia",
                latitude: "44.838769",
                longitude: "11.630187")

Department.create!(
                name: "Dipartimento di Medicina, Farmacia e Prevenzione",
                office: "Via Fossato di Mortara 70, 44121 Ferrara, Italia",
                latitude: "44.8363313",
                longitude: "11.6307579")

Department.create!(
                name: "Dipartimento di Fisica e Scienze della Terra",
                office: "Via Giuseppe Saragat 1, 44124 Ferrara, Italia",
                latitude: "44.83275",
                longitude: "11.5950753")

Department.create!(
                name: "Dipartimento di Architettura",
                office: "Via della Ghiara 36, 44121 Ferrara, Italia",
                latitude: "44.829492",
                longitude: "11.6216104")
      
Department.create!(
                name: "Dipartimento di Economia e Managment",
                office: "Via Voltapaletto 11, 44121 Ferrara, Italia",
                latitude: "44.8356356",
                longitude: "11.6224673")
          
Department.create!(
                name: "Dipartimento di Giurisprudenza",
                office: "Corso Ercole I d'Este 37, 44121 Ferrara, Italia",
                latitude: "44.845019",
                longitude: "11.622419")

Department.create!(
                name: "Dipartimento di Scienze della Vita e Biotecnologie",
                office: "Via Luigi Borsari 46, 44121 Ferrara, Italia",
                latitude: "44.8389548",
                longitude: "11.6340876")


# 100.times do |index|
#   Department.create!(
#                 name: Faker::University.name,
#                 office: Faker::Address.full_address,
#                 latitude: Faker::Address.latitude,
#                 longitude: Faker::Address.longitude)
# end

p "Created #{Department.count} Departments"
