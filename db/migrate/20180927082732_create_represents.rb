class CreateRepresents < ActiveRecord::Migration[5.2]
  def change
    create_table :represents do |t|
      t.integer :course_id
      t.integer :tag_id

      t.timestamps
    end
  end
end
