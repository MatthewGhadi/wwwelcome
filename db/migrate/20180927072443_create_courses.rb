class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :name
      t.string :course_class
      t.string :site
      t.string :access
      t.integer :students
      t.integer :duration
      t.integer :department_id
      t.string :description

      t.timestamps
    end
  end
end
