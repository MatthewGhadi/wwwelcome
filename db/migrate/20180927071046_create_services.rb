class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :name
      t.string :office
      t.string :site
      t.string :latitude
      t.string :longitude

      t.timestamps
    end
  end
end
