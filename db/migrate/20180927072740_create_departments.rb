class CreateDepartments < ActiveRecord::Migration[5.2]
  def change
    create_table :departments do |t|
      t.string :name
      t.string :office
      t.string :latitude
      t.string :longitude

      t.timestamps
    end
  end
end
