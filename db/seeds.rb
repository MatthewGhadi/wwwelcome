# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# --------> https://davidmles.com/seeding-database-rails/

#carico tutti i file .rb presenti nella cartella /db/seeds
Dir[File.join(Rails.root, 'db', 'seeds/', 'departments.rb')].sort.each do |seed|
  load seed
end
Dir[File.join(Rails.root, 'db', 'seeds/', 'services.rb')].sort.each do |seed|
  load seed
end
Dir[File.join(Rails.root, 'db', 'seeds/', 'tutorials.rb')].sort.each do |seed|
  load seed
end
Dir[File.join(Rails.root, 'db', 'seeds/', 'tags.rb')].sort.each do |seed|
  load seed
end
Dir[File.join(Rails.root, 'db', 'seeds/', 'courses.rb')].sort.each do |seed|
  load seed
end
Dir[File.join(Rails.root, 'db', 'seeds/', 'represents.rb')].sort.each do |seed|
  load seed
end
Dir[File.join(Rails.root, 'db', 'seeds/', 'users.rb')].sort.each do |seed|
  load seed
end