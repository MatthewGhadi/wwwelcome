require 'test_helper'

class ServiceTest < ActiveSupport::TestCase
  test "should find services by department lat lng" do
    service = Service.create!(
                              name: "Mensa Tecno-Polo",
                              office: "Via Giuseppe Saragat 2, 44124 Ferrara, Italia",
                              site: "http://www.unife.it/studenti/tutorato-didattico/contatti-e-orari-di-apertura",
                              latitude: "44.8357",
                              longitude: "11.5987")

    department = Department.create!(
                              name: "Dipartimento di Ingegneria",
                              office: "Via Giuseppe Saragat 1, 44124 Ferrara, Italia",
                              latitude: "44.83275",
                              longitude: "11.5950753")
    
    assert_not_empty(Service.find_services_by_department_lat_lng(department.latitude, department.longitude))
    
  end
end
