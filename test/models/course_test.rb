require 'test_helper'

class CourseTest < ActiveSupport::TestCase
  test "should find course" do
    dep = Department.create!(
                        name: "Dipartimento di Ingegneria",
                        office: "Via Giuseppe Saragat 1, 44124 Ferrara, Italia",
                        latitude: "44.83275",
                        longitude: "11.5950753")
    course = Course.create!(
                        name: "Ingegneria Informatica",
                        course_class: "LM32",
                        site: "http://ing.unife.it/lm.infoauto",
                        access: "Libero",
                        students: 300,
                        duration: 2,
                        department_id: dep.id,
                        description: "Corso Magistrale di Laure in Ingegneria Informatica e dell'Automazione")
    t1 = Tag.create!(
                        name: "#software",
                        tag_type: "C")
    t2 = Tag.create!(
                        name: "#programming",
                        tag_type: "C")
    r1 = Represent.create!(
                        course_id: course.id,
                        tag_id: t1.id)
    r2 = Represent.create!(
                        course_id: course.id,
                        tag_id: t2.id)
    assert_not_empty(Course.find_course([t1.id,t2.id]))
  end
end
