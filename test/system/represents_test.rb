require "application_system_test_case"

class RepresentsTest < ApplicationSystemTestCase
  setup do
    @represent = represents(:one)
  end

  test "visiting the index" do
    visit represents_url
    assert_selector "h1", text: "Represents"
  end

  test "creating a Represent" do
    visit represents_url
    click_on "New Represent"

    fill_in "Course", with: @represent.course_id
    fill_in "Tag", with: @represent.tag_id
    click_on "Create Represent"

    assert_text "Represent was successfully created"
    click_on "Back"
  end

  test "updating a Represent" do
    visit represents_url
    click_on "Edit", match: :first

    fill_in "Course", with: @represent.course_id
    fill_in "Tag", with: @represent.tag_id
    click_on "Update Represent"

    assert_text "Represent was successfully updated"
    click_on "Back"
  end

  test "destroying a Represent" do
    visit represents_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Represent was successfully destroyed"
  end
end
